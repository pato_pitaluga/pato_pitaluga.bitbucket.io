jQuery(function($) {
  'use strict';

  var aligning = $('#aligning');
  var audioFileName = $('#audioSource').attr('src');
  var audioFile;

  function launchSpeechAlignment() {
    aligning.removeClass('hidden');
    $('#alignmentResults').html('');

    var textFile = new Blob([$('#textSource').val()], {
      type: 'text/html'
    });

    var data = new FormData();
    data.append('audioFile', audioFile, audioFileName);
    data.append('textFile', textFile);

    $.ajax({
      method: 'POST',
      url: 'https://api-platform.systran.net/multimodal/speech/align?key=42029626-a3c6-44e3-ae76-68e5aa6efc0a&lang=es',
      data: data,
      cache: false,
      contentType: false,
      processData: false,
      success: function(data) {
        aligning.addClass('hidden');

        if (!data || !data.segments || !data.segments.length)
          return;

        var html = '';

        var outputFormatted = [];

        $.each(data.segments, function(segmentIndex, segment) {
          if (!segment.words || !segment.words.length)
            return;

          var segmentLength = parseFloat(segment.end);
          html += '<div style="display: inline-block; width: 100%; float: left; position: relative;margin-top: -40px;">';
          html += '<span class="time total">' + ' Total Time:' + segment.end + "</span>";

          $.each(segment.words, function(wordIndex, word) {
            var wordDuration = parseFloat(word.duration);

            if(!isNaN(wordDuration) && wordDuration > 0) {
              var leftMargin = 100 * parseFloat(word.start) / segmentLength;
              var width = 100 * parseFloat(word.duration) / segmentLength;

              html += '<span class="time" style="left: ' + leftMargin + '%;">' + word.start + "</span>";
              html += '<span class="word" style="width:' + width + '%; left:' + leftMargin + '%;">' + word.text + "</span>";
            }
          });
          html += '</div>';


          $.each(segment.words, function(wordIndex, word) {
            var wordDuration = parseFloat(word.duration);

            if (!isNaN(wordDuration) && wordDuration > 0) {
              var leftMargin = 100 * parseFloat(word.start) / segmentLength;
              var width = 100 * parseFloat(word.duration) / segmentLength;

              // html += '<span class="word">' + "text:" + word.text + "," +"</span>";
              // html += '<span class="time">' + "start:" + word.start + "," +"</span>";
              outputFormatted.push({
                "text": word.text.trim(),
                "start": word.start
              });
            }
          });
          html += '</div>';
        });

        document.getElementById('output').value = JSON.stringify(outputFormatted, null, 2);
        $('#alignmentResults').html(html);
      },
      error: function(xhr, status, err) {
        console.log('Error while aligning text with audio file:', err);
        aligning.addClass('hidden');
      }
    });
  }

  //Load audio file content to be sent with the request
  var xhr = new XMLHttpRequest();
  xhr.open('GET', audioFileName, true);
  xhr.responseType = 'arraybuffer';
  xhr.onload = function(e) {
    var fileContent = new Uint8Array(this.response);
    audioFile = new Blob([fileContent], {
      type: "audio/mpeg"
    });
    $('#alignButton').click(launchSpeechAlignment);
  };
  xhr.send();
});
