const fs = require('fs');
const path = require('path');

require('http').createServer((req, res) => {
  if (req.url === '/') res.end(fs.readFileSync(path.resolve(__dirname, './index.html'), 'utf8'));
  if (req.url === '/speech-align.css') res.end(fs.readFileSync(path.resolve(__dirname, './speech-align.css'), 'utf8'));
  if (req.url === '/speech-align.js') res.end(fs.readFileSync(path.resolve(__dirname, './speech-align.js'), 'utf8'));
  // if (req.url === '/audio.mp3') fs.readFile('./audio.mp3', (err, data) => res.end(data));
  if (req.url === '/favicon.ico') res.end('');
}).listen(3000, () => console.log('Listening port 3000'));
